import sys, os

""" append `app` directory to `sys.path`
"""
# absolute directory of file `test_handler.py` 
current_abspath = os.path.abspath(sys.path[0]) 

# remove `tests` from the tail of abspath `~/new-tickers-processing/app/tests`
current_abspath_list = current_abspath.split('/')
current_abspath_list.pop()

# join to form `app` directory `~/new-tickers-processing/app`
src_path = '/'.join(current_abspath_list)
sys.path.append(src_path)

import pytest
import boto3
import pandas as pd
from moto import mock_s3
from src.dispatcher import Dispatcher
from src.exceptions import TickerNotExists

stage = 'dev'

def test_get_trading_data():
    alpha = Dispatcher('fb', stage)
    result = alpha.get_trading_data('5d')
    assert str(type(result)) == "<class 'pandas.core.frame.DataFrame'>"
    assert len(result.index) == 5

    gama = Dispatcher('fb', stage)
    result = alpha.get_trading_data('max')
    assert len(result.index) > 0

    betta = Dispatcher('nobody', stage)
    result = betta.get_trading_data('5d')
    assert str(type(result)) == "<class 'pandas.core.frame.DataFrame'>"
    assert len(result.index) == 0
    assert result.empty == True

def test_is_real_ticker():
    alpha = Dispatcher('fb', stage)
    assert alpha.is_real_ticker() == True

    betta = Dispatcher('nobody', stage)
    assert betta.is_real_ticker() == False    

"""
Setup moto 
"""

@pytest.fixture(scope='function')
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    os.environ['AWS_SECURITY_TOKEN'] = 'testing'
    os.environ['AWS_SESSION_TOKEN'] = 'testing'

data = {'Name':['Tom', 'Jack', 'Steve', 'Ricky'],'Age':[28,34,29,42]}
df = pd.DataFrame(data, index=['rank1','rank2','rank3','rank4'])
csv = df.to_csv()

bucket_data = Dispatcher.uuid + '-' + stage + '-ibm'
object_key =  'ibm.csv'
        
@pytest.fixture(scope='function')
def use_moto(aws_credentials):
    with mock_s3():
        conn = boto3.resource('s3', region_name='us-east-2')
        conn.create_bucket(
            Bucket=bucket_data,
            CreateBucketConfiguration={
                'LocationConstraint': 'us-east-2'
            }
        )

        conn.Object(bucket_data,object_key).put(Body=csv)
        yield

# def test_is_followed(use_moto):
#     s3_resource = boto3.resource('s3')
#     assert s3_resource.Bucket(bucket_data).creation_date != None 
        

def test_is_followed(use_moto):
    alpha = Dispatcher('ibm',stage)
    assert alpha.is_followed() == True

    betta = Dispatcher('nobody',stage)
    assert betta.is_followed() == False

def test_save_history_trading_data(use_moto):
    alpha = Dispatcher('msft',stage)
    assert alpha.save_history_trading_data() == True

    betta = Dispatcher('nobody',stage)
    with pytest.raises(TickerNotExists):
        betta.save_history_trading_data()
