import sys, os

""" append `app` directory to `sys.path`
"""
# absolute directory of file `test_handler.py` 
current_abspath = os.path.abspath(sys.path[0]) 

# remove `tests` from the tail of abspath `~/new-tickers-processing/app/tests`
current_abspath_list = current_abspath.split('/')
current_abspath_list.pop()

# join to form `app` directory `~/new-tickers-processing/app`
src_path = '/'.join(current_abspath_list)
sys.path.append(src_path)

import subprocess
import logging
import json
import pytest
import re
from urllib.parse import urlparse
import urllib.request
import boto3
from src.dispatcher import Dispatcher

def test_staging():
    
    stage = 'dev'

    # assert sls deploys to developing environment successfully
    with open ('deploy.log', 'rt') as mf:
        contents = mf.read()

    assert 'Stack update finished' in contents
    assert ('stage: ' + stage) in contents

    # retrieve urls in endpoints and parse them
    urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', contents)
    assert urlparse(urls[0]).scheme == 'https'
    assert 'execute-api.us-east-1.amazonaws.com'  in urlparse(urls[0]).netloc
    assert urlparse(urls[0]).path == '/' + stage + '/guess'
    
    # assert lambda function has correct layers
    client = boto3.client('lambda')
    layers = client.list_layers()

    for layer in layers['Layers']:
        if layer['LayerName'] == 'yfinance':
            yfinance_layer_arn = layer['LatestMatchingVersion']['LayerVersionArn'] 

    assert client.get_function(FunctionName='new-ticker-' + stage + '-new_ticker')['Configuration']['Layers'][0]['Arn'] == yfinance_layer_arn
    
    """
    test 'https://b6blnkx556.execute-api.us-east-1.amazonaws.com/dev/guess?ticker=fb&period=5d'
    """

    t1 = 'fb'    
    url = urls[0] + '?ticker=' + t1 + '&period=5d'
    response = urllib.request.urlopen(url)
    assert response.getcode() == 200

    # confirm s3 bucket has been created successfully and object has been saved successfully
    
    bucket_name = Dispatcher.uuid + '-' + stage + '-' + t1
    obj_key = t1 + '.csv'
    s3_resource = boto3.resource('s3')
    assert s3_resource.Bucket(bucket_name).creation_date != None
    assert len(s3_resource.Object(bucket_name, obj_key).get()['Body'].read().decode('utf-8')) > 0

    # clean up s3 bucket which was created by lambda during the staging test
    bucket = s3_resource.Bucket(bucket_name)
    for key in bucket.objects.all():
        key.delete()
    bucket.delete()
    