import boto3
import logging
from src.exceptions import TickerNotExists
from botocore.exceptions import ClientError
import yfinance as yf
import pandas as pd
from io import StringIO

class Dispatcher:
    """ This class is used to process new ticker guess request
    """

    # https://bitbucket.org/share-investing/new-ticker/issues/1/ticker-buckets-and-object-naming
    uuid = 'fen9li-share-investing-v3'
    
    Logger = logging.getLogger(__name__)

    def __init__(self, ticker, stage):
        self.ticker = ticker.lower()
        self.stage = stage.lower()
        self.bucket_data = Dispatcher.uuid + '-' + self.stage + '-' + self.ticker
        self.object_key = self.ticker + '.csv'
        
    def save_history_trading_data(self):
        """Save the stock history trading data

        :return: True if success
        """ 
        if self.is_real_ticker():
            if self.is_followed():
                pass
            else:
                self.create_bucket()
                hist = self.get_trading_data('max')

                csv_buffer = StringIO()
                hist.to_csv(csv_buffer)

                self.save_bucket_object_contents(csv_buffer.getvalue())
                return True
        else: 
            Dispatcher.Logger.error(self.ticker + ': TickerNotExists')
            raise TickerNotExists(self.ticker, 'TickerNotExists')

    def save_bucket_object_contents(self, contents:str) -> (bool):
        """Save contents to self.object_key in S3 bucket self.bucket_data

        :param contents: String contents to save to bucket object
        :return: True if success
        """

        try:
            s3_resource = boto3.resource('s3')
            s3_resource.Object(self.bucket_data, self.object_key).put(Body=contents)
        except ClientError as e:
            Dispatcher.Logger.error(e)
            return False

        return True

    def create_bucket(self, region=None) -> (bool):
        """Create a S3 bucket in a specified region

        If a region is not specified, the bucket is created in the S3 default
        region (us-east-1).

        :param region: String region to create bucket in, e.g., 'us-west-2'
        :return: True if bucket created successfully, else False
        """

        try:
            if region is None:
                s3_client = boto3.client('s3')
                s3_client.create_bucket(Bucket=self.bucket_data)
            else:
                s3_client = boto3.client('s3', region_name=region)
                location = {'LocationConstraint': region}
                s3_client.create_bucket(Bucket=self.bucket_data,
                                        CreateBucketConfiguration=location)
        except ClientError as e:
            Dispatcher.Logger.error(e)
            return False
        return True

    def is_followed(self) -> (bool):
        """Return True if the ticker has data bucket, False if not

        :return: True or False
        """
        s3_resource = boto3.resource('s3')
       
        if s3_resource.Bucket(self.bucket_data).creation_date == None: 
            return False
        else:
            return True

    def is_real_ticker(self) -> (bool):
        """Return True with real ticker, False with fake ticker

        :return: True or False
        """
        
        return False if self.get_trading_data('1d').empty else True
        
    def get_trading_data(self,days:str) -> (pd.DataFrame):
        """Gets and returns the stock history trading data

        :return: history trading data in <class 'pandas.core.frame.DataFrame'> format
        """
        return yf.Ticker(self.ticker).history(period=days)
    




