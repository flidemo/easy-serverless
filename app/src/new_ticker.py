import json
from src.dispatcher import Dispatcher

def new_ticker(event, context):
    
    ticker = event['queryStringParameters']['ticker']
    period = event['queryStringParameters']['period']
    stage = event['requestContext']['stage']

    peter = Dispatcher(ticker, stage)
    if peter.save_history_trading_data(): 
        report = ticker + ': daily trading data saved successfully'

    response = {
        'statusCode': 200,
        'body': json.dumps(report)
    }

    return response
