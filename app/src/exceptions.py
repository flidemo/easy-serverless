# define share investing v3 exceptions
class Error(Exception):
    """Base error for other exceptions"""
    pass


class TickerNotExists(Error):
    """Raised when the ticker history daily trading data is Empty"""
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors
        print(errors)

class TickerNotFollowed(Error):
    """Raised when the ticker history daily trading data is not Empty but no research result"""
    pass
