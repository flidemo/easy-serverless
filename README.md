## Execlusive Summary

This is a demo AWS Serverless application. 
A Lambda function is triggered by API Gateway, and then accesses yahoo finance api to retrieve stock daily trading data, if not yet retrieved, and finally, saves the daily trading data to an object in a S3 bucket.

## Architecture

![architecture](images/architecture.png)

1. Visitor raises http(s) request to API Gateway.
2. API gateway proxies the request and triggers Lambda function. 
3. Lambda function queries AWS Management API to find out if ticker's trading data storage S3 bucket exists.
4. Lambda function gets the query response (s3 bucket existing situation will be handled in other project).
5. Lambda function queries yfinance API to retrieve ticker's daily trading data.
6. Lambda function gets the query response.
7. Lambda function creates a new s3 bucket and saves ticker's daily trading data to s3 bucket.
8. Lambda responds to visitor.

## Prerequites
* Build jenkins server and create pipeline

## Deploy `yfinance` layer 

> Note: this is a one time off task which should not send to pipeline. Follow below step to deploy it manually.

```
git clone git@bitbucket.org:flidemo/easy-serverless.git
cd easy-serverless

[fli@koala easy-serverless]$ python38 -m venv .venv
[fli@koala easy-serverless]$ source .venv/bin/activate
(.venv) [fli@koala guess-ticker]$ tree layers/
layers/
├── serverless.yml
└── yfinance
    └── tools
        └── aws_requirements.txt

2 directories, 2 files
(.venv) [fli@koala guess-ticker]$ cd layers/yfinance/tools/
(.venv) [fli@koala tools]$ ll
total 4
-rw-rw-r--. 1 fli fli 29 Sep 24 14:23 aws_requirements.txt
(.venv) [fli@koala tools]$  

pip install -t python/lib/python3.8/site-packages -r aws_requirements.txt

(.venv) [fli@koala tools]$

(.venv) [fli@koala layers]$ pwd
/home/fli/guess-ticker/layers
(.venv) [fli@koala layers]$ ll
total 4
-rw-rw-r--. 1 fli fli 339 Sep 24 14:21 serverless.yml
drwxrwxr-x. 3 fli fli  19 Sep 24 14:20 yfinance
(.venv) [fli@koala layers]$  

sls deploy --stage dev

(.venv) [fli@koala layers]$
```

![create-finance-layer-01](images/create-finance-layer-01.png)
![create-finance-layer-02](images/create-finance-layer-02.png)

## Deploy to production

> Note: push to `master` branch will trigger deploying to production environment

![deploy-to-prod-01](images/deploy-to-prod-01.png)

![deploy-to-prod-02](images/deploy-to-prod-02.png)

![deploy-to-prod-03](images/deploy-to-prod-03.png)

![deploy-to-prod-04](images/deploy-to-prod-04.png)
```
[fli@koala ~]$ aws s3 cp s3://fen9li-share-investing-v3-prod-dji/dji.csv Downloads/ --profile default
download: s3://fen9li-share-investing-v3-prod-dji/dji.csv to Downloads/dji.csv
[fli@koala ~]$ head -n 5 Downloads/dji.csv 
Date,Open,High,Low,Close,Volume,Dividends,Stock Splits
1970-01-02,809.2,809.2,809.2,809.2,0,0,0
1970-01-05,811.31,811.31,811.31,811.31,0,0,0
1970-01-06,803.66,803.66,803.66,803.66,0,0,0
1970-01-07,801.81,801.81,801.81,801.81,0,0,0
[fli@koala ~]$ tail -n 5 Downloads/dji.csv 
2020-09-18,27657.4,27657.4,27657.4,27657.4,0,0,0
2020-09-21,27147.7,27147.7,27147.7,27147.7,0,0,0
2020-09-22,27288.2,27288.2,27288.2,27288.2,0,0,0
2020-09-23,26763.1,26763.1,26763.1,26763.1,0,0,0
2020-09-24,26815.4,26815.4,26815.4,26815.4,0,0,0
[fli@koala ~]$ 
```
